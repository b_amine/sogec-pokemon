<?php

namespace App\Command;

use App\Entity\Pokemon;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsCommand(name: 'app:create-test-user')]
class CreateTestUserCommand extends Command
{
    protected static $defaultName = 'app:create-test-user';

    private EntityManagerInterface $entityManager;
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(EntityManagerInterface $entityManager, UserPasswordHasherInterface $passwordHasher)
    {
        $this->entityManager = $entityManager;
        $this->passwordHasher = $passwordHasher;
        parent::__construct();
    }

    public function configure(): void
    {
        $this->setDescription('Create test user.')
            ->setHelp('This command creates a test user with credentials "test@test.fr" and "TestTest@"');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $user = new User();
        $user->setEmail("test@test.fr");
        $password = $this->passwordHasher->hashPassword($user, "TestTest@");
        $user->setPassword($password);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return Command::SUCCESS;
    }


}
