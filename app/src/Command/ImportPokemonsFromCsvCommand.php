<?php

namespace App\Command;

use App\Entity\Pokemon;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


#[AsCommand(name: 'app:import-pokemons-csv')]
class ImportPokemonsFromCsvCommand extends Command
{
    protected static $defaultName = 'app:import-pokemons-csv';

    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    public function configure(): void
    {
        $this->setDescription('Import Pokemons.')
            ->setHelp('This command imports pokemons from a CSV file');

        $this->addArgument('fileName', InputArgument::REQUIRED, 'file name');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $fileName = $input->getArgument('fileName');
        $file = fopen('./data/'.$fileName, 'r');

        if($file) {
            $output->writeln("<info>Greetings trainer ! Your Pokémons are being imported !</info>");

            // This array will be used to determine the index of each column, because CSV files won't always have the same column order
            $indexes = [];

            // Get line of column names, verify none is missing and assign them to indexes array
            $cols = fgetcsv($file);

            $check =  $this->checkCols($cols, $indexes);

            if($check !== false) {
                $output->writeln("Colonne '".$check. "' manquante");
                return Command::FAILURE;
            }

            $count = 0;
            while(($line = fgetcsv($file)) !== false) {
                $pokemon = new Pokemon();
                $pokemon->setName($line[$indexes['Name']]);
                $pokemon->setType1($line[$indexes['Type 1']]);
                $pokemon->setType2($line[$indexes['Type 2']]);
                $pokemon->setTotal($line[$indexes['Total']]);
                $pokemon->setHp($line[$indexes['HP']]);
                $pokemon->setAttack($line[$indexes['Attack']]);
                $pokemon->setDefense($line[$indexes['Defense']]);
                $pokemon->setSpAtk($line[$indexes['Sp. Atk']]);
                $pokemon->setSpDef($line[$indexes['Sp. Def']]);
                $pokemon->setSpeed($line[$indexes['Speed']]);
                $pokemon->setGeneration($line[$indexes['Generation']]);

                if(!in_array($line[$indexes['Legendary']], ['True','False'])) {
                    $output->writeln("Valeur invalide, 'True','False' seulement acceptés");
                    return Command::FAILURE;
                }
                $pokemon->setLegendary($line[$indexes['Legendary']] === 'True');
                $this->entityManager->persist($pokemon);

                $count++;
            }


            // Save changes to DB
            $this->entityManager->flush();

            //Close file & return success
            fclose($file);

            $output->writeln("<info>Sucess ! ".$count. " Pokémons have been added to your Pokédex !</info>");
            return Command::SUCCESS;
        } else {
            $output->writeln("File not found");
            return Command::FAILURE;
        }
    }

    private function checkCols(array $cols, array &$indexes): bool|string
    {

        $indexes['Name'] = array_search('Name', $cols);
        $indexes['Type 1'] = array_search('Type 1', $cols);
        $indexes['Type 2'] = array_search('Type 2', $cols);
        $indexes['Total'] = array_search('Total', $cols);
        $indexes['HP'] = array_search('HP', $cols);
        $indexes['Attack'] = array_search('Attack', $cols);
        $indexes['Defense'] = array_search('Defense', $cols);
        $indexes['Sp. Atk'] = array_search('Sp. Atk', $cols);
        $indexes['Sp. Def'] = array_search('Sp. Def', $cols);
        $indexes['Speed'] = array_search('Speed', $cols);
        $indexes['Generation'] = array_search('Generation', $cols);
        $indexes['Legendary'] = array_search('Legendary', $cols);

        return array_search(false, $indexes);
    }
}
