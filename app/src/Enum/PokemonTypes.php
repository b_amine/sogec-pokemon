<?php

namespace App\Enum;

enum PokemonTypes
{
    case Grass;
    case Fire;
    case Water;
    case Bug;
    case Normal;
    case Poison;
    case Electric;
    case Ground;
    case Fairy;
    case Fighting;
    case Psychic;
    case Rock;
    case Ghost;
    case Ice;
    case Dragon;
    case Dark;
    case Steel;
    case Flying;
}