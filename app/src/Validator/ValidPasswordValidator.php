<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class ValidPasswordValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof ValidPassword) {
            throw new UnexpectedTypeException($constraint, ValidPassword::class);
        }


        if (null === $value || '' === $value) {
            return;
        }

        if (!is_string($value)) {
            throw new UnexpectedValueException($value, 'string');
        }

        $regex_pattern =  '/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[;,?:.\/@#"\'{}\[\]\-_\(\)\$*\%=+]).{8,}$/';

        if (!preg_match($regex_pattern, $value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $value)
                ->addViolation();
        }
    }
}
