<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class ValidPassword extends Constraint
{
    public string $message = 'Invalid password : Password must contain at least 8 characters, one upper case letter, one lower case letter and a symbol : ,;:?./@#"\'{}[]-_()$*%=+';

    public function __construct(
        array $groups = null,
        mixed $payload = null,
    ) {
        parent::__construct([], $groups, $payload);
    }

}
