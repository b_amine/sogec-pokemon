# API POKEMON :

## INSTALLATION
Pour installer le projet, lancer :
<br> `docker-compose up` <br>
Puis installer les packages :
<br> `docker-compose exec php composer install` <br>
Puis créer les tables : <br> 
`docker-compose exec php php bin/console doctrine:migrations:migrate`

## IMPORT :
Pour importer les données, lancer la commande : <br>
`docker-compose exec php php bin/console app:import-pokemons-csv NOM_FICHIER'` <br>
en remplaçant <b>NOM_FICHIER</b> par le nom d'un fichier du dossier <b>data</b>

## API

URL : `localhost:9999/api`

### Creation d'utilisateurs :
Soit lancer la commande <br>
`docker-compose exec php php bin/console app:create-test-user` 
<br> Cette commande crée un user avec les identifiants : <b>test@test.fr / TestTest@</b>

Soit passer par la route /register : <br> `POST localhost:9999/api/register` <br>
Il faut un JSON de cette forme : <br> `{"email":"test2@test.fr", "password":"TestTest@"}`

### Authentification
Créer la clé privée et publique : <br>
`docker-compose exec php php bin/console lexik:jwt:generate-keypair`
<br>

La suppression et modification de Pokémon sont protegées par une authentification JWT.
Pour les utiliser il faut d'abord se connecter en appelant la route : <br>
`POST localhost:9999/api/login_check` <br>
avec un JSON de cette forme : <br>`{"username":"test2@test.fr", "password":"TestTest@"}`
L'api retournera un token qu'il faudra passer dans le header : <br>
`{
"Authorization" : "Bearer {token}"
}`<br>
En remplaçant <b>{token}</b> par le token retourné par l'API.

### Gestion des pokemons :

#### Récupérer tous les pokemons :
` GET localhost:9999/api/pokemon` <br>
L'api retourne par défaut la première page avec 50 pokemons par page. On peut spécifier le numero de la page à afficher 
ainsi que le nombre de Pokémons souhaité par page en ajoutant les paramètres <b>itemsPerPage</b> et <b>page</b>. Exemple : <br>
`localhost:9999/api/pokemon?itemsPerPage=10&page=5`

On a aussi l'option de filtre, On peut filtrer selon les champs suivants :
- name (string)
- type1 (string),
- type2 (string)
- generation (int)
- legendary (bool) => mettre true ou false, ou bien 1 ou 0

Pour le <b>name</b>, On peut écrire une partie ou la totalité du nom, ce qui permet de rechercher et de filter.

Exemple : <br>
`GET localhost:9999/api/pokemon?legendary=1,generation=2` <br>


#### Récupérer un pokemon par ID : 
`GET localhost:9999/api/pokemon/{id}` <br>
Remplacer <b>{id}</b> par l'identifiant du Pokémon dans cet exemple et tous les suivants.

#### Modifier un pokémon : 
`PUT localhost:9999/api/pokemon/{id}` <br>
Cet endpoint gère les modifications partielles & totales. Il faut passer en body un JSON qui contient au moins un de ces 5 champs : <br>
- name (string) 
- type1 (string),
- type2 (string)
- generation (int)
- legendary (bool)

Le type doit être parmi les types déjà existants 

#### Supprimer un pokémon :
`DELETE localhost:9999/api/pokemon/{id}` <br>

<b><u>On ne peut ni supprimer ni modifier un Pokémon legendaire</u></b>

